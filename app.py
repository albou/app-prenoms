import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output

app = dash.Dash(__name__)
server = app.server

fichier_prenoms = ('https://gitlab.com/albou/app-prenoms/raw/master/nat2016.txt')
liste_prenoms = pd.read_csv(fichier_prenoms, sep="\t", header=0)
# liste_prenoms["prenom"] = liste_prenoms["preusuel"].str.lower()
liste_prenoms = liste_prenoms[liste_prenoms["annais"]!="XXXX"]
liste_prenoms = liste_prenoms[liste_prenoms["annais"]!="XXXX"]
liste_menu = liste_menu.groupby(["prenom","preusuel"])["nombre"].sum()
liste_menu = pd.DataFrame(liste_menu).reset_index()
liste_menu = liste_menu[liste_menu["nombre"]>100]  # On ne propose que les prénoms ayant plus de 100 entrées
liste_menu = liste_menu["prenom"].unique()

app.layout = html.Div(children=[
    html.H1(children='Popularité des prénoms donnés à la naissance'),
    dcc.Dropdown(
        id='liste_defil',
        options=[{'label': i.capitalize(), 'value': i} for i in liste_menu],
        value='Alban',
        placeholder="Saisir un prénom ici",
    ),
    html.Div([
        dcc.Graph(id='graphe')
    ])

])

@app.callback(
    Output(component_id='graphe', component_property='figure'),
    [Input(component_id='liste_defil', component_property='value')]
)
def update_output_div(input_value):
    recherche = input_value.lower()
    # recherche = unicodedata.normalize('NFKD', recherche).encode('ascii', 'ignore').decode()
    donnees = liste_prenoms[liste_prenoms["prenom"]==recherche]
    donnees = donnees[(donnees["annais"]!="XXXX")]
    donnees = donnees.groupby(["annais"])["nombre"].sum()

    intervalle_annees = pd.DataFrame(data = {"années" : np.arange(1900,2017,1)})
    indices = [int(i) for i in donnees.index]
    donnees_prenom = pd.DataFrame(data={"années" : indices, "nombre":donnees})
    intervalle_annees = intervalle_annees.merge(donnees_prenom,how = "left",on="années")
    return {
            'data': [
                    go.Bar(
                        x=list(intervalle_annees["années"]),
                        y=list(intervalle_annees["nombre"])
                    )],
            'layout': go.Layout(
                title={'title': input_value},
                xaxis={'title': 'Années'},
                yaxis={'title': 'Nombre de naissance'},
                margin={'l': 40, 'b': 100, 't': 10, 'r': 50},
                legend={'x': 0, 'y': 1},
                hovermode='closest'
            )
    }

if __name__ == '__main__':
    app.run_server(debug=True)